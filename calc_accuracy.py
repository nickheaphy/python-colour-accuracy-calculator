# Calculate the different between two files

# First file should be formated:
# L A B Name
# Second file should be formated:
# L A B

import math
import os
import sys

class ColorList:
    def __init__(self):
        self.colours = []

    def returnMaxDE(self):
        big = 0.0
        name = ""
        for colour in self.colours:
            if colour.dE > big:
                big = colour.dE
                name = colour.name
        return name, big

    def returnMinDE(self):
        small = 100.0
        name = ""
        for colour in self.colours:
            if colour.dE < small:
                small = colour.dE
                name = colour.name
        return name, small

    def returnTotal(self):
        return len(self.colours)

    def returnOutOfGamut(self, limit):
        count = 0
        for colour in self.colours:
            if colour.dE > limit:
                count += 1
        return count

    def returnOutOfGamutPercent(self, limit):
        return self.returnOutOfGamut(limit)/self.returnTotal()*100

    def returnDistribution(self):
        maxspots = 20
        distribution = [0] * maxspots
        for colour in self.colours:
            if math.floor(colour.dE) < maxspots-1:
                distribution[math.floor(colour.dE)] += 1
            else:
                distribution[maxspots-1] += 1
        return distribution

    def list_all_colours(self):
        sorted_name= sorted(self.colours, key= lambda e:e.name)
        for color in sorted_name:
            print(f"{color.name} - dE00 = {color.dE:.1f}")



class ColorDifference:
    def __init__(self, colorname, dE):
        self.name = colorname
        self.dE = dE

def __lt__(self, other):
         return self.name < other.name

def dE76(l1,a1,b1,l2,a2,b2):
    '''Calculate the deltaE76 between two lab values'''
    # http://www.brucelindbloom.com/index.html?Eqn_DeltaE_CIE94.html
    return math.sqrt((l2-l1)**2 + (a2-a1)**2 + (b2-b1)**2)

def dE94(l1,a1,b1,l2,a2,b2):
    '''Calculate the deltaE94 between two lab values'''
    # http://www.brucelindbloom.com/index.html?Eqn_DeltaE_CIE94.html
    dl = l1 - l2
    c1 = math.sqrt(a1**2 + b1**2)
    c2 = math.sqrt(a2**2 + b2**2)
    dc = c1 - c2
    da = a1 - a2
    db = b1 - b2
    dh = math.sqrt(abs(da**2 + db**2 - dc**2))
    kl = 1
    kc = 1
    kh = 1
    k1 = 0.045
    k2 = 0.015
    sl = 1
    sc = 1 + k1 * c1
    sh = 1 + k2 * c1
    return math.sqrt((dl/(kl*sl))**2 + (dc/(kc*sc))**2 + (dh/(kh*sh))**2)

def dE2000(l1,a1,b1,l2,a2,b2):
    '''Calculate the difference between reference 1 and sample 2'''
    # http://www.brucelindbloom.com/index.html?Eqn_DeltaE_CIE2000.html
    # Hope this is right as it is a nasty formula
    Lp = (l1 + l2) / 2
    C1 = math.sqrt(a1**2 + b1**2)
    C2 = math.sqrt(a2**2 + b2**2)
    C = (C1 + C2) / 2
    G = 0.5 * (1 - math.sqrt(C**7/(C**7 + 25**7)))
    a1p = a1 * (1 + G)
    a2p = a2 * (1 + G)
    C1p = math.sqrt(a1p**2 + b1**2)
    C2p = math.sqrt(a2p**2 + b2**2)
    Cp = (C1p + C2p) / 2
    h1p = (math.degrees(math.atan2(b1,a1p)) + 360) % 360
    h2p = (math.degrees(math.atan2(b2,a2p)) + 360) % 360
    if abs(h1p - h2p) > 180:
        Hp = (h1p + h2p + 360) / 2
    else:
        Hp = (h1p + h2p) / 2
    T = (1 - 0.17 * math.cos(math.radians(Hp - 30)) +
        0.24 * math.cos(math.radians(2 * Hp)) +
        0.32 * math.cos(math.radians(3*Hp + 6)) -
        0.20 * math.cos(math.radians(4 * Hp - 63)))
    if abs(h2p - h1p) <= 180:
        dhp = h2p - h1p
    elif abs(h2p - h1p) > 180 and h2p <= h1p:
        dhp = h2p - h1p + 360
    else:
        dhp = h2p - h1p - 360
    dLp = l2 - l1
    dCp = C2p - C1p
    dHp = 2 * math.sqrt(C1p*C2p) * math.sin(math.radians(dhp/2))
    SL = 1 + ((0.015 * (Lp - 50)**2) / math.sqrt(20 + (Lp - 50)**2))
    SC = 1 + 0.045 * Cp
    SH = 1 + 0.015 * Cp * T
    dTheta = 30 * math.exp(-((Hp - 275)/25)**2)
    RC = 2 * math.sqrt(Cp**7/(Cp**7+25**7))
    RT = -RC * math.sin(math.radians(2*dTheta))
    KL = 1
    KC = 1
    KH = 1
    return math.sqrt(
        (dLp/(KL*SL))**2 +
        (dCp/(KC*SC))**2 +
        (dHp/(KH*SH))**2 +
        RT *
        (dCp/(KC*SC)) *
        (dHp/(KH*SH)) )



def main(orig, conv):
    
    orig_lab = open(orig,'r').readlines()
    conv_lab = open(conv,'r').readlines()

    colorlist = ColorList()

    for i, line in enumerate(orig_lab):
        l1 = line.split(' ')[0]
        a1 = line.split(' ')[1]
        b1 = line.split(' ')[2]
        l2 = conv_lab[i].split(' ')[0]
        a2 = conv_lab[i].split(' ')[1]
        b2 = conv_lab[i].split(' ')[2]
        dE = dE2000(float(l1),float(a1),float(b1),float(l2),float(a2),float(b2))
        colourname = ' '.join(line.split(' ')[3:]).strip()
        colorlist.colours.append(ColorDifference(colourname,dE))
        
        #if dE > 2.3:
        #    print("%s %s" % (colourname, dE))
    
    # http://johnthemathguy.blogspot.co.nz/2017/07/is-10-delta-e-just-noticeable-difference.html
    # http://zschuessler.github.io/DeltaE/learn/

    print("")
    print("Total PMS Colours: %s" % (colorlist.returnTotal()))
    print("Total Out of Gamut (dE2000 > 1): %s (%.1f%%)" % (colorlist.returnOutOfGamut(1),colorlist.returnOutOfGamutPercent(1)))
    print("Max dE2000: %s dE2000=%.1f" % (colorlist.returnMaxDE()))
    # print("Min dE2000: %s dE2000=%.1f" % (colorlist.returnMinDE()))
    dist = colorlist.returnDistribution()
    for i, val in enumerate(dist):
        if i == 0:
            print(f"dE2000 < {i+1} = {val} ({(val/colorlist.returnTotal()*100):.1f}%)")
        elif i == len(dist)-1:
            print(f"dE2000 > {i} = {val} ({(val/colorlist.returnTotal()*100):.1f}%)")
        else:
            print(f"dE2000 {i} to {i+1} = {val} ({(val/colorlist.returnTotal()*100):.1f}%)")

    print("dE <= 1.0 Not perceptible by human eyes.")
    print("dE 1 - 2 Perceptible through close observation.")
    print("dE 2 - 10 Perceptible at a glance.")
    print("dE 11 - 49 Colors are more similar than opposite.")
    print("dE 100 Colors are opposite.")
    print("")

    colorlist.list_all_colours()



# ------------------------------
if __name__ == "__main__":
    # Check the passed parameters
    dE94 = dE94(67.8,57.3,63.3,75.5,28.5,30.4)
    dE76 = dE76(67.8,57.3,63.3,75.5,28.5,30.4)
    dE2k = dE2000(11.989890,19.316406,-34.800781,20.998519,16.268007,-32.464180)
    # print("DE76 = %s", dE76)
    # print("DE94 = %s", dE94)
    print("DE2k = %s", dE2k)
    if len(sys.argv) != 3:
        print("Need to pass the two files on the commandline")
        exit()
    
    if not os.path.isfile(sys.argv[1]):
        print("Can't open the first file")
        exit(1)
    
    if not os.path.isfile(sys.argv[2]):
        print("Can't open the second file")
        exit(1)

    print(f"Comparing {sys.argv[1]} to {sys.argv[2]}")
    print(f"Please see https://bitbucket.org/nickheaphy/python-colour-accuracy-calculator for calculation details.")
    print(f"Note that the colour conversions are using Argyll Colour Management Engine which will be slightly different")
    print(f"to what your printer is likely using. This will mean the exact dE might be different on your device.")
    print("")
    main(sys.argv[1], sys.argv[2])

