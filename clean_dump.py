# Clean up a dump produced by iccdump

# Open the file on the command line

# Read line by line. Look for the text 'Color 0:' which will indicate the start
# of the PMS data

# Data looks like:
#    Color 0:
#      Name root = 'PANTONE 3514 C'
#      Lab = 78.969056, 14.417969, 91.769531

import sys
import os

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

class Spotcolour():
    def __init__(self, colorname):
        self.name = colorname
        self.L = 0.0
        self.A = 0.0
        self.B = 0.0


def main(iccdump):
    data = []

    with open(iccdump, "r") as iccfile:
        found0 = False
        found1 = False

        for line in iccfile:
            if found0 == False:
                #print(line.strip())
                if line.strip()[:6] == "Color ":
                    #print(line.strip()[7])
                    if RepresentsInt(line.strip()[6]): 
                        # need to grab the next two lines
                        found0 = True
                    
            else:
                if found1 == False:
                    # This line contains the Pantone name
                    # "Name root = 'PANTONE Warm Gray 5 C'"
                    cleanname = line.strip().split('=')[1].strip().replace("'","")
                    data.append(Spotcolour(cleanname))
                    found1 = True
                else:
                    # This line contains the LAB colour
                    # Lab = 78.969056, 14.417969, 91.769531
                    #print(line)
                    numbers_only = line.strip().split('=')[1]
                    lab = numbers_only.split(',')
                    #print(lab)
                    data[-1].L = lab[0].strip()
                    data[-1].A = lab[1].strip()
                    data[-1].B = lab[2].strip()
                    found0 = False
                    found1 = False
            
        for spot in data:
            print("%s %s %s %s" % (spot.L, spot.A, spot.B, spot.name))
                
                







# ------------------------------
if __name__ == "__main__":
    # Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the dump file on the commandline")
        exit()
    
    if not os.path.isfile(sys.argv[1]):
        print("Can't open the dump file")
        exit(1)

    main(sys.argv[1])

