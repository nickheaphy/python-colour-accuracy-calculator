#!/bin/bash

# This will need to be updated based on the location and ICC profiles being used.

pp=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )


# Extract the LAB from the named profile
"$pp"/Argyll_V2.2.1/bin/iccdump -v 3 "$pp"/Profiles/PANTONE\ PLUS\ Solid\ V3/Solid\ Coated-V3.icc > ICCDumpCoated.txt
"$pp"/Argyll_V2.2.1/bin/iccdump -v 3 "$pp"/Profiles/PANTONE\ PLUS\ Solid\ V3/Solid\ Uncoated-V3.icc > ICCDumpUncoated.txt

# Clean up Argyll data
python3 clean_dump.py ICCDumpCoated.txt > SpotLibrary-Coated-Lab.txt
python3 clean_dump.py ICCDumpUncoated.txt > SpotLibrary-Uncoated-Lab.txt

# Convert the lab values to CMYK using the output profile
"$pp"/Argyll_V2.2.1/bin/icclu -fb -pl -ir -v0 -s100 "$pp"/Profiles/DME_CoatedSilk_PremiumQuality_iX_R4.1_EU.icc < SpotLibrary-Coated-Lab.txt > SpotLibrary-Coated-CMYK.txt
"$pp"/Argyll_V2.2.1/bin/icclu -fb -pl -ir -v0 -s100 "$pp"/Profiles/DME_Uncoated_PremiumQuality_iX_R4.1.1_EU.icc < SpotLibrary-Uncoated-Lab.txt > SpotLibrary-Uncoated-CMYK.txt
"$pp"/Argyll_V2.2.1/bin/icclu -fb -pl -ir -v0 -s100 "$pp"/Profiles/DME+_Uncoated130-350_CG50DS1_PremiumQuality_M0_130gm2.icc < SpotLibrary-Uncoated-Lab.txt > SpotLibrary-Uncoated-CMYKDMEPlus.txt

# Now convert the CMYK values back into Lab
"$pp"/Argyll_V2.2.1/bin/icclu -ff -pl -ir -v0 -s100 "$pp"/Profiles/DME_CoatedSilk_PremiumQuality_iX_R4.1_EU.icc < SpotLibrary-Coated-CMYK.txt > SpotLibrary-Coated-Lab-Reversed.txt   
"$pp"/Argyll_V2.2.1/bin/icclu -ff -pl -ir -v0 -s100 "$pp"/Profiles/DME_Uncoated_PremiumQuality_iX_R4.1.1_EU.icc < SpotLibrary-Uncoated-CMYK.txt > SpotLibrary-Uncoated-Lab-Reversed.txt
"$pp"/Argyll_V2.2.1/bin/icclu -ff -pl -ir -v0 -s100 "$pp"/Profiles/DME+_Uncoated130-350_CG50DS1_PremiumQuality_M0_130gm2.icc < SpotLibrary-Uncoated-CMYKDMEPlus.txt > SpotLibrary-Uncoated-Lab-ReversedDMEPlus.txt

# Report
python3 calc_accuracy.py SpotLibrary-Coated-Lab.txt SpotLibrary-Coated-Lab-Reversed.txt > SpotLibrary-CoatedPremium-Report.txt
python3 calc_accuracy.py SpotLibrary-Uncoated-Lab.txt SpotLibrary-Uncoated-Lab-Reversed.txt > SpotLibrary-UncoatedPremium-Report.txt
python3 calc_accuracy.py SpotLibrary-Uncoated-Lab.txt SpotLibrary-Uncoated-Lab-ReversedDMEPlus.txt > SpotLibrary-UncoatedPremiumDMEPlus-Report.txt

# Create a visualisations
#"$pp"/Argyll_V2.2.1/bin/iccgamut -w -ff -ia -d 2.0 "$pp"/Profiles/DME_CoatedSilk_PremiumQuality_iX_R4.1_EU.icc

# Delete the Temp Files
rm -f ICCDumpCoated.txt ICCDumpUncoated.txt
rm -f SpotLibrary-Coated-Lab.txt SpotLibrary-Coated-CMYK.txt SpotLibrary-Coated-Lab-Reversed.txt
rm -f SpotLibrary-Uncoated-Lab.txt SpotLibrary-Uncoated-CMYK.txt SpotLibrary-Uncoated-Lab-Reversed.txt
rm -f SpotLibrary-Uncoated-CMYKDMEPlus.txt SpotLibrary-Uncoated-Lab-ReversedDMEPlus.txt


