# Python Colour Accuracy Calculator

Creates a report on an ICC profile to determine how many in-gamut Pantone Colours there are.

It does this by first converting the Pantone LAB values to CMYK using the output profile of the device (this will produce the closest match that the output profile is capable of), then converting these CMYK values back to LAB, then you calculate the difference between the two LAB values.

## Installation

The scripts require Python3 and [ArgyllCms](https://www.argyllcms.com) to work.

#### Python

First install Python3

##### Mac OSX

Use https://brew.sh/ as per screenshots below. Once Brew is installed enter `brew install python3`)

![Brew Install Screenshot](/inc/brew_install.png)

If using OSX and you already have Brew installed, update to the latest Python3 version with `brew upgrade python3`

#### ArgyllCms

Install LittleCMS using Brew `brew install argyll-cms`

## Using

Download a ICC spot colour profile (eg [Fiery Pantone Librarys](https://www.efi.com/products/fiery-servers-and-software/fiery-workflow-suite/fiery-color-and-imaging/pantone-libraries/)) to extract the PMS LAB values from.

Dump the ICC profile as text using `iccdump -v 3 Profile.icc > ICCDump.txt` (this will list all the spot colours and their LAB values)

Clean the dump and output a cleaned text file using `python3 clean_dump.py ICCDump.txt > Spot_list.txt` (this just reformats so it can be consumed by `icclu`)

Ensure you have an `output.icc` that describes the device and stock you want to report on.

Convert the `Spot_list.txt` to CMYK with the output profile using `icclu -fb -pl -ir -v0 -s100 output.icc < Spot_list.txt > CMYK_list.txt`

Convert the `CMYK_list.txt` to LAB uwith the output profile using `icclu -ff -pl -ir -v0 -s100 output.icc < CMYK_list.txt > Spot_list2.txt`

Calculate the difference between the two lists of LAB colours using `python3 calc_accuracy.py Spot_list.txt Spot_list2.txt`






